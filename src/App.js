
import './App.css';
import 'antd/dist/antd.css';
import AgoraRTC from 'agora-rtc-sdk';
import { Row, Col } from 'antd';
import { Component } from 'react';
import BasicSet from './components/BasicSet';
import VideoList from './components/VideoList';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      localStream: null,
      remoteStreams: [],
    };
  }

  updateLocalStream(newLocalStream) {
    this.setState({ localStream: newLocalStream });
  }

  freshRemoteStreams(currentRemoteStreams) {
    this.setState({ remoteStreams: currentRemoteStreams });
  }

  addRemoteStreams(newStream) {
    let oldRemoteStreams = this.state.remoteStreams;
    oldRemoteStreams.push(newStream);
    this.setState({ remoteStreams: oldRemoteStreams });

  }

  removeRemoteStreams(streamWillBeRemoved) {
    let id = streamWillBeRemoved.getId();
    let currentRemoteStreams = this.state.remoteStreams.filter(function (stream) {
      return stream.getId() !== id
    })
    this.setState({ remoteStreams: currentRemoteStreams });
  }

  componentDidMount() {

  }

  render() {
    console.log(AgoraRTC);
    return (
      <Row className="app">
        <Col flex="300px">
          <BasicSet
            params={{
              localStream: this.state.localStream,
              remoteStreams: this.state.remoteStreams,
            }}

            updateLocalStream={(_newLocalStream) => { this.updateLocalStream(_newLocalStream) }}
            freshRemoteStreams={(_currentRemoteStreams) => { this.freshRemoteStreams(_currentRemoteStreams) }}
            addRemoteStreams={(_newStream) => { this.addRemoteStreams(_newStream) }}
            removeRemoteStreams={(_streamWillBeRemoved) => { this.removeRemoteStreams(_streamWillBeRemoved) }}
          />
        </Col>
        <Col flex="auto">
          <VideoList
            params={{
              localStream: this.state.localStream,
              remoteStreams: this.state.remoteStreams,
            }}
          />
        </Col>
      </Row>
    );
  }
}

export default App;
