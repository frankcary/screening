import { Component } from 'react';

export default class Main extends Component {

    componentDidUpdate() {
        if (this.props.localStream) {
            this.props.localStream.play("local_stream");
        }
    }

    render() {
        return <>
            {
                this.props.localStream ?
                    <div className="video-view">
                        <div id="local_stream" className="video-placeholder"></div>
                        <div id="local_video_info" className="video-profile hide"></div>
                        <div id="video_autoplay_local" className="autoplay-fallback hide"></div>
                    </div>
                    : null
            }
        </>;
    }
}