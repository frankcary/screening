import { Component } from 'react';

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.id = null;
        if (this.props.stream) {
            this.id = this.props.stream.getId();
        }
    }

    componentDidMount() {
        if (this.props.stream) {
            this.props.stream.play("remote_video_" + this.id);
        }
    }

    componentDidUpdate() {
        if (this.props.stream) {
            this.props.stream.play("remote_video_" + this.id);
        }
    }

    render() {
        return <div className="video-view" id={'remote_video_' + this.id}></div>;
    }
}