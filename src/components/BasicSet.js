import { Component } from 'react';
import { Row, Col, Input, Button } from 'antd';
import AgoraRTC from 'agora-rtc-sdk';
import trim from 'trim';

// agora Code Error https://docs.agora.io/en/API%20Reference/the_error_web

export default class Main extends Component {

    constructor(props) {
        super(props);

        // appID for debug 
        this.state = {
            appID: '',
            channel: '',
            token: '',
            joined: false,
            published: false,
            client: null,
            uid: null,
        };
    }

    handleVideoEvent(_client) {

        // Occurs when an error message is reported and requires error handling.
        _client.on("error", (err) => {
            console.log(err)
        })
        // Occurs when the peer user leaves the channel; for example, the peer user calls Client.leave.
        _client.on("peer-leave", (evt) => {
            var id = evt.uid;
            console.log("id", evt)
            let streams = this.props.params.remoteStreams.filter(e => id !== e.getId())
            let peerStream = this.props.params.remoteStreams.find(e => id === e.getId())
            if (peerStream && peerStream.isPlaying()) {
                peerStream.stop()
            }

            // rtc.remoteStreams = streams
            console.log('current stream', streams);
            this.props.freshRemoteStreams(streams);

            if (id !== this.state.uid) {
                // TODO removeView(id)
            }
            console.log("peer-leave", id)
        })
        // Occurs when the local stream is published.
        _client.on("stream-published", (evt) => {
            console.log("stream-published")
        })
        // Occurs when the remote stream is added.
        _client.on("stream-added", (evt) => {
            var remoteStream = evt.stream
            var id = remoteStream.getId();
            if (id !== this.state.uid) {
                _client.subscribe(remoteStream, (err) => {
                    console.log("stream subscribe failed", err)
                })
            }
            console.log("stream-added remote-uid: ", id)
        })
        // Occurs when a user subscribes to a remote stream.
        _client.on("stream-subscribed", (evt) => {
            var remoteStream = evt.stream
            var id = remoteStream.getId();
            if (id !== this.state.uid) {
                this.props.addRemoteStreams(remoteStream);
            }

            // TODO addView(id)
            // TODO remoteStream.play("remote_video_" + id)
            console.log("stream-subscribed remote-uid: ", id)
        })
        // Occurs when the remote stream is removed; for example, a peer user calls Client.unpublish.
        _client.on("stream-removed", (evt) => {
            var remoteStream = evt.stream
            var id = remoteStream.getId()
            if (remoteStream.isPlaying()) {
                remoteStream.stop()
            }
            this.props.removeRemoteStreams(remoteStream);
            // rtc.remoteStreams = rtc.remoteStreams.filter(function (stream) {
            //     return stream.getId() !== id
            // })
            // removeView(id)
            console.log("stream-removed remote-uid: ", id)
        })
        _client.on("onTokenPrivilegeWillExpire", function () {
            // After requesting a new token
            // rtc.client.renewToken(token);
            console.log("onTokenPrivilegeWillExpire")
        })
        _client.on("onTokenPrivilegeDidExpire", function () {
            // After requesting a new token
            // client.renewToken(token);
            console.log("onTokenPrivilegeDidExpire")
        })
    }

    async join() {

        if (this.state.joined) {
            console.error("Your already joined")
            return;
        }

        if (!this.validate()) {
            console.error("parameters is not right")
        }


        let client = AgoraRTC.createClient({ mode: 'live', codec: 'h264' });

        this.setState({ client });

        this.handleVideoEvent(client);

        let initResp = await this.initPromis(client);
        if (initResp) {
            console.log("init success")

            let uid = await this.joinPromis(client, this.state.token, this.state.channel, this.state.uid);

            this.setState({
                joined: true,
                uid,
            });

            const localStream = AgoraRTC.createStream({
                streamID: uid,
                audio: true,
                video: true,
                screen: false,
                uid,
            })

            // initialize local stream. Callback function executed after intitialization is done
            localStream.init(() => {
                this.props.updateLocalStream(localStream);
                // publish local stream
                this.publish()
            }, function (err) {
                console.error("init local stream failed ", err)
            })
        } else {
            console.error(initResp);
        }
    }

    initPromis(_client) {
        return new Promise((resolve, reject) => {
            _client.init(this.state.appID, () => {
                resolve(true);
            }, (err) => {
                console.log(err);
                reject(false, err);
            });
        });
    }

    joinPromis(_client, token, channel, uid) {

        return new Promise((resolve, reject) => {

            _client.join(token ? token : null, channel, uid, (uid) => {
                resolve(uid);
            }, function (err) {
                reject(err);
            });
        });
    }

    unpublish() {
        if (!this.state.client) {
            console.error("Please Join Room First")
            return
        }
        if (!this.state.published) {
            console.error("Your didn't publish")
            return
        }


        var oldState = this.state.published;
        this.unpublishPromis()
            .then(() => {
                this.setState({
                    published: false,
                });
            }).catch((err) => {
                this.setState({
                    published: oldState,
                });
                console.log("unpublish failed")
                console.error(err)
            });
    }

    unpublishPromis() {
        return new Promise((resolve, reject) => {
            this.state.client.unpublish(this.props.params.localStream, (err) => {
                // rtc.published = oldState
                console.log("unpublish failed")
                console.error(err)
                reject(err);
            })
            resolve();
        });
    }

    publish() {
        if (!this.state.client) {
            console.error("Please Join Room First")
            return
        }
        if (this.state.published) {
            console.error("Your already published")
            return
        }
        var oldState = this.state.published;

        this.publishPromis()
            .then(() => {
                console.info("publish")
                // rtc.published = true
                this.setState({
                    published: true,
                });
            }).catch((err) => {
                this.setState({
                    published: oldState,
                });
                console.log("unpublish failed")
                console.error(err)
            });
    }

    publishPromis() {
        return new Promise((resolve, reject) => {
            this.state.client.publish(this.props.params.localStream, (err) => {
                // rtc.published = oldState
                console.log("unpublish failed")
                console.error(err)
                reject(err);
            })
            resolve();
        });
    }

    leave() {
        if (!this.state.client) {
            console.error("Please Join First!")
            return
        }
        if (!this.state.joined) {
            console.error("You are not in channel")
            return
        }
        /**
         * Leaves an AgoraRTC Channel
         * This method enables a user to leave a channel.
         **/
        this.state.client.leave(() => {
            // stop stream
            if (this.props.params.localStream.isPlaying()) {
                this.props.params.localStream.stop()
            }
            // close remote stream
            this.props.params.localStream.close()
            for (let i = 0; i < this.props.params.remoteStreams.length; i++) {
                let stream = this.props.params.remoteStreams[i];
                if (stream.isPlaying()) {
                    stream.stop()
                }
            }

            // rtc.localStream = null
            this.props.updateLocalStream(null);

            // rtc.remoteStreams = []
            this.props.freshRemoteStreams([]);

            // rtc.client = null
            // rtc.published = false
            // rtc.joined = false
            this.setState({
                published: false,
                joined: false,
                client: null,
            });
            console.log("client leaves channel success")
        }, function (err) {
            console.log("channel leave failed")
            console.error("leave success")
            console.error(err)
        })
    }

    validate() {

        if (!trim(this.state.appID)) {
            console.error('appID is empty');
            return false;
        }

        if (!trim(this.state.channel)) {
            console.error('channel is empty');
            return false;
        }

        if (!trim(this.state.token)) {
            console.error('token is empty');
            return false;
        }

        return true;
    }

    componentDidMount() {

    }

    changeAppId(event) {
        let val = event.target.value;
        this.setState({
            appID: val,
        });
    }

    changeChannel(event) {
        let val = event.target.value;
        this.setState({
            channel: val,
        });
    }

    changeToken(event) {
        let val = event.target.value;
        this.setState({
            token: val,
        });
    }


    render() {
        return <>
            <Row>
                <Col span={24}>
                    <Input placeholder="App ID" onChange={(eve) => { this.changeAppId(eve) }} />
                </Col>
            </Row>
            <br />
            <Row>
                <Col span={24}>
                    <Input placeholder="Channel" onChange={(eve) => { this.changeChannel(eve) }} />
                </Col>
            </Row>
            <br />
            <Row>
                <Col span={24}>
                    <Input placeholder="Token" onChange={(eve) => { this.changeToken(eve) }} />
                </Col>
            </Row>
            <br />
            <Row>
                <Col span={6}>
                    <Button type="primary" onClick={(eve) => { this.join() }}>Join</Button>
                </Col>
                <Col span={6}>
                    <Button type="primary" onClick={(eve) => { this.leave() }}>Leave</Button>
                </Col>
                <Col span={6}>
                    <Button type="primary" onClick={(eve) => { this.publish() }}>Publish</Button>
                </Col>
                <Col span={6}>
                    <Button type="primary" onClick={(eve) => { this.unpublish() }}>Unpublish</Button>
                </Col>
            </Row>
        </>;
    }
}