import { Component } from 'react';
import LocalVideo from './LocalVideo';
import RemoteVideo from './RemoteVideo';

export default class Main extends Component {
    render() {
        let videoList = [];
        console.log('list video');
        this.props.params.remoteStreams.forEach(stream => {
            console.log('streamId', stream.getId());
            videoList.push(<RemoteVideo stream={stream} key={stream.getId()} />);
        });

        return <div>
            <LocalVideo localStream={this.props.params.localStream} />

            {videoList}
        </div>;
    }
}